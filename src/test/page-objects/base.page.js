export default class Page {
  constructor() {}

  open(path) {
    browser.url(path);
  }

  get title() { return browser.getTitle(); }
  get url() { return browser.getUrl(); }
  get source() { return browser.getSource(); }
}
