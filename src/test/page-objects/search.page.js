import Page from './base.page';

export default class SearchPage extends Page {
  constructor() {
    super();
  }

  get searchField() { return $('[name="q"]'); }
  get searchButton() { return $('#search_button_homepage'); }

  searchFor(searchTerm) {
    this.searchField.setValue(searchTerm);
    this.searchButton.click();
  }
}
