import SearchResultsPage from './search-results.page';

export default class RedditPage extends SearchResultsPage {
  constructor() {
    super();
  }
  get promotedLink() { return $('.promotedlink'); }
}
