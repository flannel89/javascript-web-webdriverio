import SearchPage from './search.page';

export default class SearchResultsPage extends SearchPage {
  constructor() {
    super();
  }

  get results() { return $('#links'); }
  get resultsLinks() { return $$('.result__a'); }

  selectResultAtIndex(index) {
    this.results.isVisible();
    this.resultsLinks[index].click();
  }
}
