import { Given, When, Then } from 'cucumber';
import Page from '../page-objects/base.page';

const page = new Page();

Given(/^I am on the (?:base|search) page$/, () => {
  page.open('/');
});

Then(/^I see the page title contains "([^"]*)"$/, (expectedPageTitle) => {
  expect(page.title).to.contain(expectedPageTitle);
});

Then(/^I see the page Url contains "([^"]*)"$/, (expectedPageUrl) => {
  expect(page.url).to.contain(expectedPageUrl);
});

Then(/^I see "([^"]*)" in the PageSource$/, (expectedPageSource) => {
  expect(page.source).to.contain(expectedPageSource);
});

Then(/^I see$/, (dataTable) => {
  dataTable.rows().forEach(row => {
    expect(page.source).to.contain(row);
  });
});
