import { Given, When, Then } from 'cucumber';
import searchResultsPage from '../page-objects/search-results.page';

When(/^I view the first result$/, () => {
  const search = new searchResultsPage();
  search.selectResultAtIndex(0);
});
