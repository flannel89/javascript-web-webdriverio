import { Given, When, Then } from 'cucumber';
import searchPage from '../page-objects/search.page';

When(/^I search for "([^"]*)"$/, (searchTerm) => {
  console.log("here");

  const search = new searchPage();
  console.log(search);
  search.searchFor(searchTerm);
});
